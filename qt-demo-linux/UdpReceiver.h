#ifndef UDPRECIEVER_H
#define UDPRECIEVER_H
#include <QObject>
#include <QUdpSocket>

class UdpReceiver : public QObject

{
    Q_OBJECT
public:
    UdpReceiver(QObject *p=0);
    ~UdpReceiver();
public slots:
    void receive();
private:
    QUdpSocket *uSocket;
};

#endif // UDPRECEIVER_H
