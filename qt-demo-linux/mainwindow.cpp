﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "seedreader.cpp"
#include <QApplication>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QMessageBox>
#include <QProgressBar>

#include <QMessageBox>
#include <QByteArray>
#include "seed.h"
const quint16 PORT = 8888;

const QString InfoPath= "res/scored_torrent_information.csv";
const QString LogPath="res/log.txt";
const QString sUrl="http://localhost:9091/transmission/web/";


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    uSocket = new QUdpSocket;
    uSocket->bind(QHostAddress("127.0.0.1"), PORT);
    connect(uSocket, SIGNAL(readyRead()), this, SLOT(UdpReceive()));

    model=new QStandardItemModel();
    //定义表头
    QStringList labels =  QObject::trUtf8("Seed Name,Torrent Name,Size,Price,Score").split(",");
    model->setHorizontalHeaderLabels(labels);
    ui->tableView->setModel(model);


    LogText=new QTextBrowser(this);       //待放置到tabWidget中的控件
    QHBoxLayout *TabCommandlayout=new QHBoxLayout(this);     //包裹控件的布局
    TabCommandlayout->setContentsMargins(0,0,0,0);
    TabCommandlayout->addWidget(LogText);
    ui->LogTab->setLayout(TabCommandlayout);

      myWeb = new QWebEngineView(this);
      //myWeb = new QAxWidget(this);
      //设置ActiveX控件为IEMicrosoft Web Browser
      //设置ActiveX控件的id，最有效的方式就是使用UUID
      //此处的{8856F961-340A-11D0-A96B-00C04FD705A2}就是Microsoft Web Browser控件的UUID
      //myWeb->setControl(QString::fromUtf8("{8856F961-340A-11D0-A96B-00C04FD705A2}"));
      //myWeb->setObjectName(QString::fromUtf8("webWidget"));//设置控件的名称
      //myWeb->setFocusPolicy(Qt::StrongFocus);//设置控件接收键盘焦点的方式：鼠标单击、Tab键
      //myWeb->setProperty("DisplayAlerts",false); //不显示任何警告信息。
      //myWeb->setProperty("DisplayScrollBars",true); // 显示滚动条
      QHBoxLayout *TabWeblayout=new QHBoxLayout(this);     //包裹控件的布局
      TabWeblayout->setContentsMargins(0,0,0,0);
      TabWeblayout->addWidget(myWeb);
      ui->InfoTab->setLayout(TabWeblayout);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_LoginButton_clicked()
{

    if(ui->LoginButton->text()=="登录")
    {
        if(LoginCheck())
        {
            ui->PassWordLable->hide();
            ui->PassWordEdit->hide();
            ui->NameEdit->setReadOnly(true);
            ui->NameEdit->setStyleSheet("background-color: rgb(186, 186, 186);");
            ui->LoginButton->setText("退出");
            //webWidget->dynamicCall("Navigate(const QString&)",sUrl);
            //myWeb->setUrl(QUrl(sUrl));
	    myWeb->load(QUrl(sUrl));

	    myWeb->show();
        }

    }
    else
    {
        ui->PassWordLable->show();
        ui->PassWordEdit->show();
        ui->NameEdit->setStyleSheet("background-color: rgb(255, 255, 255);");
        ui->NameEdit->setReadOnly(false);
        ui->LoginButton->setText("登录");

    }

}

bool MainWindow::LoginCheck()
{
    if (ui->NameEdit->text()=="HYD0525" && ui->PassWordEdit->text()=="HYD112358")
    return true;
    else {
        QMessageBox::critical(this , "critical message" , "用户名或密码错误", QMessageBox::Ok | QMessageBox::Default,0);
        return  false;
    }
}



void MainWindow::UdpReceive()
{
    QByteArray ba;
    while(uSocket->hasPendingDatagrams())
    {
        ba.resize(uSocket->pendingDatagramSize());
        uSocket->readDatagram(ba.data(), ba.size());
        if (ba.data()=="update successfully")
        {
            RefreshInfo();
        }
    }

}

void  MainWindow:: RefreshInfo()
{
    seeds * seedinfos= readcsvfile(InfoPath);
    if (item1!=NULL){
    delete item1;
    delete item2;
    delete item3;
    delete item4;
    delete item5;
            }
    for(int i = 0;i < 100;i++){
        item1 = new QStandardItem(   seedinfos[i].get_seed_name());

        model->setItem(i,0,item1);
//QString::fromUtf8
        item2 = new QStandardItem(  seedinfos[i].get_torrent_name());
        model->setItem(i,1,item2);
        item3 = new QStandardItem(seedinfos[i].get_size());
        model->setItem(i,2,item3);
        item4 = new QStandardItem(QString::number(seedinfos[i].get_price()));
        model->setItem(i,3,item4);
        item5 = new QStandardItem(seedinfos[i].get_score());
        model->setItem(i,4,item5);

    }
}



void MainWindow::on_RefreshLogButton_clicked()
{
    RefreshInfo();
    QString displayString;
    QFile file(LogPath);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"Can't open the file!"<<endl;
    }
    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str(line);
        displayString.append(str);
    }
       LogText->clear();
       LogText->setPlainText(displayString);
}

