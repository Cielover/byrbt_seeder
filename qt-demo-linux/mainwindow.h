#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include <QStandardItemModel>

#include <QStackedLayout>
#include <QTextBrowser>
#include <QWebEngineView>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void RefreshInfo();

private slots:
    void on_LoginButton_clicked();//登录按钮
    bool LoginCheck();
    void UdpReceive();
    void on_RefreshLogButton_clicked();//登录按钮

private:
    Ui::MainWindow *ui;
    QUdpSocket *uSocket;
    QStandardItem* item1=NULL;
    QStandardItem* item2=NULL;
    QStandardItem* item3=NULL;
    QStandardItem* item4=NULL;
    QStandardItem* item5=NULL;
    QStandardItemModel *model;
    QWebEngineView  *myWeb ;
    //QWebEngineView  *myWeb = new QWebEngineView(this)
    QTextBrowser* LogText;


};
#endif // MAINWINDOW_H
