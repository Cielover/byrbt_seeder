#include <QApplication>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QDebug>
#include <QMessageBox>
#include <QTextCodec>
#include "seed.h"
seeds* readcsvfile(QString path)
{
	
    seeds *read_seeds= new seeds[100];
    QFile csvFile(path);
    QStringList csvList;
    csvList.clear();
    if (csvFile.open(QIODevice::ReadOnly)) //对csv文件进行读写操作
    {
        QTextStream stream(&csvFile);
        while (!stream.atEnd())
        {
            csvList.push_back(stream.readLine()); //保存到List当中
        }
        csvFile.close();
    }
    else
    {
        QMessageBox::about(NULL, "csv文件", "未打开该文件！");
    }
    int i = 0;
    Q_FOREACH(QString str, csvList)   //遍历List
    {
        i = i + 1;
        //得到种子信息
        if (i>1){
            
            QStringList valsplit = str.split(","); //分隔字符串
            QString seed_name = valsplit[1];
            QString torrent_name= valsplit[2];
            QString size= valsplit[3];
            int price = valsplit[4].toInt();
            QString score= valsplit[5];
            read_seeds[i-2].set_rank(i);
            read_seeds[i-2].set_seed_name(seed_name);
            read_seeds[i-2].set_torrent_name(torrent_name);
            read_seeds[i-2].set_size(size);
            read_seeds[i-2].set_price(price);
            read_seeds[i-2].set_score(score);
        }
    }
    return read_seeds;
}
