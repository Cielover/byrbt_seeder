#ifndef SEED_H
#define SEED_H
#include<QString>

struct seed_info
{
    int rank;
    QString seed_name;
    QString torrent_name;
    int price;
    QString size;
    QString score;
};


class seeds
{
private:
    int rank;
    QString seed_name;
    QString torrent_name;
    int price;
    QString size;
    QString score;
public:
    seeds(int rank,QString seed_name, QString torrent_name, int price, QString size, QString score);
    seeds();
    ~seeds();
    int get_rank();
    QString get_seed_name();
    QString get_torrent_name();
    int get_price();
    QString get_size();
    QString get_score();

    void set_rank(int rank);
    void set_seed_name(QString seed_name);
    void set_torrent_name(QString torrent_name);
    void set_price(int price);
    void set_size(QString size);
    void set_score(QString score);
};

seeds::seeds(int rank,QString seed_name, QString torrent_name, int price, QString size, QString score)
{
    this->rank=rank;
    this->seed_name=seed_name;
    this->torrent_name=torrent_name;
    this->price=price;
    this->size=size;
    this->score=score;
}


seeds::~seeds(){}
seeds::seeds(){}
int seeds::get_rank(){return rank;}
QString seeds::get_seed_name(){return seed_name;}
QString seeds::get_torrent_name(){return torrent_name;}
int seeds::get_price(){return price;}
QString seeds::get_size(){return size;}
QString seeds::get_score(){return score;}


void seeds::set_rank(int rank){this->rank=rank;}
void seeds::set_seed_name(QString seed_name){this->seed_name=seed_name;}
void seeds::set_torrent_name(QString torrent_name){this->torrent_name= torrent_name;}
void seeds::set_price(int price){this->price= price;}
void seeds::set_size(QString size){this->size= size;}
void seeds::set_score(QString score){this->score= score;}


#endif // SEED_H
