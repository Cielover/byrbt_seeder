/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QWidget *UserInfowidget;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QFrame *UserInfoframe;
    QVBoxLayout *verticalLayout_4;
    QWidget *UserInfoWidget;
    QFormLayout *formLayout_3;
    QLineEdit *NameEdit;
    QLabel *PassWordLable;
    QLineEdit *PassWordEdit;
    QLabel *NameLabel;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *LoginButton;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *verticalSpacer;
    QWidget *SeedWidget;
    QVBoxLayout *verticalLayout_3;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QTableView *tableView;
    QTabWidget *tabWidget;
    QWidget *InfoTab;
    QWidget *LogTab;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *RefreshLogButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(882, 641);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setStyleSheet(QStringLiteral(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        UserInfowidget = new QWidget(centralwidget);
        UserInfowidget->setObjectName(QStringLiteral("UserInfowidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(UserInfowidget->sizePolicy().hasHeightForWidth());
        UserInfowidget->setSizePolicy(sizePolicy1);
        UserInfowidget->setMinimumSize(QSize(250, 0));
        UserInfowidget->setMaximumSize(QSize(250, 16777215));
        UserInfowidget->setStyleSheet(QStringLiteral(""));
        verticalLayout = new QVBoxLayout(UserInfowidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        UserInfoframe = new QFrame(UserInfowidget);
        UserInfoframe->setObjectName(QStringLiteral("UserInfoframe"));
        UserInfoframe->setStyleSheet(QStringLiteral(""));
        UserInfoframe->setFrameShape(QFrame::StyledPanel);
        UserInfoframe->setFrameShadow(QFrame::Raised);
        verticalLayout_4 = new QVBoxLayout(UserInfoframe);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        UserInfoWidget = new QWidget(UserInfoframe);
        UserInfoWidget->setObjectName(QStringLiteral("UserInfoWidget"));
        formLayout_3 = new QFormLayout(UserInfoWidget);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        NameEdit = new QLineEdit(UserInfoWidget);
        NameEdit->setObjectName(QStringLiteral("NameEdit"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, NameEdit);

        PassWordLable = new QLabel(UserInfoWidget);
        PassWordLable->setObjectName(QStringLiteral("PassWordLable"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, PassWordLable);

        PassWordEdit = new QLineEdit(UserInfoWidget);
        PassWordEdit->setObjectName(QStringLiteral("PassWordEdit"));
        PassWordEdit->setEchoMode(QLineEdit::Password);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, PassWordEdit);

        NameLabel = new QLabel(UserInfoWidget);
        NameLabel->setObjectName(QStringLiteral("NameLabel"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, NameLabel);


        verticalLayout_4->addWidget(UserInfoWidget);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_4);

        LoginButton = new QPushButton(UserInfoframe);
        LoginButton->setObjectName(QStringLiteral("LoginButton"));
        LoginButton->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(LoginButton->sizePolicy().hasHeightForWidth());
        LoginButton->setSizePolicy(sizePolicy2);
        LoginButton->setMinimumSize(QSize(100, 0));

        horizontalLayout_4->addWidget(LoginButton);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_5);


        verticalLayout_4->addLayout(horizontalLayout_4);


        verticalLayout->addWidget(UserInfoframe);

        verticalSpacer = new QSpacerItem(20, 418, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addWidget(UserInfowidget);

        SeedWidget = new QWidget(centralwidget);
        SeedWidget->setObjectName(QStringLiteral("SeedWidget"));
        verticalLayout_3 = new QVBoxLayout(SeedWidget);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        widget = new QWidget(SeedWidget);
        widget->setObjectName(QStringLiteral("widget"));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(-1, -1, -1, 0);
        tableView = new QTableView(widget);
        tableView->setObjectName(QStringLiteral("tableView"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tableView->sizePolicy().hasHeightForWidth());
        tableView->setSizePolicy(sizePolicy3);

        verticalLayout_2->addWidget(tableView);

        tabWidget = new QTabWidget(widget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        InfoTab = new QWidget();
        InfoTab->setObjectName(QStringLiteral("InfoTab"));
        tabWidget->addTab(InfoTab, QString());
        LogTab = new QWidget();
        LogTab->setObjectName(QStringLiteral("LogTab"));
        tabWidget->addTab(LogTab, QString());

        verticalLayout_2->addWidget(tabWidget);


        verticalLayout_3->addWidget(widget);

        widget_2 = new QWidget(SeedWidget);
        widget_2->setObjectName(QStringLiteral("widget_2"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy4);
        widget_2->setMinimumSize(QSize(0, 0));
        widget_2->setMaximumSize(QSize(16777215, 16777215));
        horizontalLayout_2 = new QHBoxLayout(widget_2);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 15, 0);
        horizontalSpacer = new QSpacerItem(535, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        RefreshLogButton = new QPushButton(widget_2);
        RefreshLogButton->setObjectName(QStringLiteral("RefreshLogButton"));
        QSizePolicy sizePolicy5(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(RefreshLogButton->sizePolicy().hasHeightForWidth());
        RefreshLogButton->setSizePolicy(sizePolicy5);
        RefreshLogButton->setMaximumSize(QSize(28, 28));
        RefreshLogButton->setStyleSheet(QLatin1String("image: url(:/res/refresh.png);\n"
"background-image: url(:/res/refresh.png);"));

        horizontalLayout_2->addWidget(RefreshLogButton);


        verticalLayout_3->addWidget(widget_2);


        horizontalLayout->addWidget(SeedWidget);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 882, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        PassWordLable->setText(QApplication::translate("MainWindow", "\345\257\206\347\240\201\357\274\232", Q_NULLPTR));
        NameLabel->setText(QApplication::translate("MainWindow", "\347\224\250\346\210\267\345\220\215\357\274\232", Q_NULLPTR));
        LoginButton->setText(QApplication::translate("MainWindow", "\347\231\273\345\275\225", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(InfoTab), QApplication::translate("MainWindow", "\347\275\221\351\241\265\345\261\225\347\244\272", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(LogTab), QApplication::translate("MainWindow", "\346\227\245\345\277\227\346\226\207\344\273\266", Q_NULLPTR));
        RefreshLogButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
